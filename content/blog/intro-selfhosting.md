---
title: 'O que é selfhosting e por que adotá-lo'
date: 2021-05-28T16:00:00-03:00
draft: false
keywords: ["devops", "computando-arte", "texto", "raspberry-pi","selfhost"]
tags:  ["devops", "computando-arte",  "texto", "raspberry-pi", "selfhost"]
---

Obs.: Originalmente publicado no [computando-arte]({{< ref "/blog/computando-arte" >}}) dia 28Maio2021

Muitos dos serviços digitais que utilizamos no nosso dia a dia estão na "nuvem", como Gmail, LinkedIn, Google Drive/Docs etc ... e como diria um famoso ditado “essa tal de "nuvem" não existe, são apenas computadores de outras pessoas”. Mas, então quais as implicações da maioria das nossas vidas digitais estar nesses computadores dessas grandes empresas? Essas implicações no geral são positivas para os usuários?

{{< figure src="fsfe-nocloud.jpg" width=400px >}}

Neste texto, que vai ser o primeiro de uma série, vamos falar de uma alternativa simples chamada self hosting, ou seja, hospedar os serviços que usamos em computadores que de fato temos controle, e como fazê-lo.

## Motivos para adotar o self-hosting

Dentre todos os motivos para adotar o self-hosting, vale destacar os seguintes:

### Segurança

Com muita frequência, vemos notícias de vazamentos envolvendo nossos dados, e mesmo assim muitos dos vazamentos não são percebidos pelas próprias empresas e logo não são noticiados, ou seja, a quantidade de vazamentos pode ser ainda maior.


{{< figure src="dados-vazam.jpg" width=400px >}}
 
Hospedando as próprias coisas não é um alvo tão grande para os cibercriminosos quanto das big tech, pense nos incentivos, se comprometer uma big tech o cibercriminoso vai ter os dados de milhões de pessoas , enquanto comprometer sua pequena infra não rendera tanto dados assim. Além disso outro incentivo para atacar as big tech é que muitas delas tem programas de bug bounty para recompensar a descoberta da suas inseguranças.

E também isso vai te ensinar quais são as boas práticas de segurança e como adota-lá.

### Privacidade

Outro ditado famoso diz que se o serviço é gratuito, o produto é você. E isso é verdade, você está pagando o serviço com seus dados.

Com selfhosting, seus dados ficam apenas com você, retomando totalmente sua privacidade, afinal foi você quem montou o esquemas de criptografia de disco, gerenciamento de senhas (afinal como as senhas sempre vazam não pode usar a mesma senha em mais de um lugar, é bom se inscrever no [haveibeenpwned.com](https://haveibeenpwned.com/) para ser notificado se suas senhas vazaram)

### Aprendizado e experiência

Pra min essa é a principal vantagem, hospedar suas próprias coisas vai proporcionar um conhecimento e experiência enorme em diversas disciplinas como:

- Sistemas operacionais (principalmente Linux e talvez BSD).
- Redes
- Segurança
- Contêineres
- Linguagens de script
- Infraestrutura como código
- Monitoramento
- E principalmente Inglês

Ter o seu "ambiente controlado" vai te possibilitar aprender, testar e quebrar as coisas que te proporcionará um aprendizado enorme que num ambiente profissional do trabalho muitas vezes não te permitiria.

## Exemplos de aplicações

As aplicações mais comuns que normalmente são hospedadas são:

- [Pi-Hole](https://pi-hole.net/): Permite bloquear anúncios, domínios que te rastreia e maliciosos a nível de rede, ou seja em todos seus dispositivos não apenas no navegador.
- [syncthing](https://syncthing.net/): É um app pra Android no PC que permite sincronizar pastas do seu android com o PC e vice versa. Dessa forma você consegue fazer backup do seu celular e sincronizar arquivos sem precisar de uma nuvem das big tech. Se tiver interesse leia meu texto de como montei meu setup de backups: [Como parei de me preocupar e passei a adorar minha solução de backups](https://caioau.net/blog/backups/).
- [nextcloud](https://nextcloud.com/): É uma solução de nuvem completa, permite sincronizar arquivos como dropbox, sincronizar contatos e agenda, quadro kanban (tipo trello), videochamadas e muito mais.
- [Home Assistant](https://www.home-assistant.io/): Gosta da sua alexa ou google assistant controlando suas luzes e outras coisas da sua smart home? Com o Home assistant você consegue ter seu próprio hub de automação com total privacidade e controle. Gosta de eletrônica? O projeto [esphome](https://esphome.io/) consegue integrar suas plaquinhas ESP8266/ESP32  com o Home Assistant.

## Considerações finais

Com o selfhosting você terá privacidade e controle total dos seus dados e vai aprender muito nessa jornada, nós próximos textos vamos abordar de fato começar.

Vale uma pequena ressalva, casos você seja uma pessoa dev e só quer colocar seu app no ar, e seu objetivo principal agora é apenas isso, talvez ir pelo caminho do selfhosting não seja a melhor alternativa agora, administração de sistemas é muitas vezes um trabalho de tempo integral e talvez seja melhor usar serviços como [heroku](https://www.heroku.com/), [vercel](https://vercel.com/) e [netlify](https://www.netlify.com/) que cuidaram de tudo para você, para que consiga fazer essa entrega mais rápido. E quanto tiver disponibilidade estudar e adotar o selfhosting :P
