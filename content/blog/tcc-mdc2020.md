---
title: "Trabalho final: Curso aprimoramento mineração de dados complexos"
date: 2020-09-03T18:52:53-03:00
draft: false
keywords: ["unicamp", "curso", "ciencia-de-dados"]
tags: ["unicamp", "curso", "ciencia-de-dados"]
---

Estou disponibilizando trabalho final que fizemos no curso de mineração de dados complexos: [pagina do curso](https://www.ic.unicamp.br/~mdc/)

É o problema do kaggle: [New York City Taxi Trip Duration](https://www.kaggle.com/c/nyc-taxi-trip-duration).

* [Video da apresentação](https://www.youtube.com/watch?v=2PGkU5M2Mos&list=PLwNGlbMOyZP2d0NT_g2yyYHqXqFo7n0r6&index=7&t=0s)
* [Relatório final](DataTroopers-RelatorioFinal-NYCtaxi.pdf)
