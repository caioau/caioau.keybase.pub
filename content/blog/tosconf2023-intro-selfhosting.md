---
title: "Slides: Primeiros passos com self-hosting (TOSCONF[3])"
date: 2023-03-17T08:00:00-03:00
draft: false
keywords: ["LHC", "slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
tags:  ["LHC","slides", "devops", "raspberry-pi","selfhost", "linux", "docker", "ansible", "security", "monitoramento"]
---

Atividade realizada dia 18 março 2023, na [TOSCONF[3]](https://tosconf.lhc.net.br/) no [Laboratório Hacker de Campinas – LHC](https://lhc.net.br/)

Palestra baseada no texto [Primeiros passos com self-hosting]({{< ref "/blog/selfhosting" >}})

* [slides](slides-intro-selfhosting_18mar2023.pdf) ([arquivo markdown editável](https://gitlab.com/caioau/caioau.gitlab.io/-/tree/master/static/blog/tosconf2023-intro-selfhosting) feito com [Marp](https://marp.app/)).
