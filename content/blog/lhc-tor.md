---
title: "Slides: Cebolizando: O que é Tor? Como usar? E como contribuir!"
date: 2019-12-12T16:10:14-03:00
draft: false
keywords: ["LHC", "slides", "Tor"]
tags: ["LHC", "slides", "Tor"]
---

Atividade realizada dia 12 dezembro 2019, no [Laboratório Hacker de Campinas – LHC](https://lhc.net.br/)

* [slides](TorLHC2019.pdf) ([codigo fonte dos slides](https://github.com/caioau/caioau-personal/blob/master/website/slides/TorLHC2019.md))
