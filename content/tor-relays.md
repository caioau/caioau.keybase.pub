---
title: "Tor relays"
date: 2023-05-3
type: "page"
tags: ["Tor"]
keywords: ["Tor"]
---

Colabore com a rede Tor, rode um relay! Aqui a pagina com as instruções: [Relay Operations](https://community.torproject.org/relay/)

## Bridges

* [SemAnistia](https://metrics.torproject.org/rs.html#details/928AE65A36D1DD77BC54131A0AAE06E1793CBFB4)
* [Atlantis](https://metrics.torproject.org/rs.html#details/ACB66A66515D52CF01F365D6011EA40D3DD68822)
* [OperationPlate](https://metrics.torproject.org/rs.html#details/365BD00DCCA58B3FD69571BDD692D4FF28CA2823)
