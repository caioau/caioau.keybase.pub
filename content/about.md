---
title: "About"
date: 2023-03-20T08:00:00-03:00
type: "page"
tags: ["Signal", "PGP", "Twitter", "about", "contact", "unicamp", "friends"]
keywords: ["Signal", "PGP", "Twitter", "about", "contact", "unicamp", "friends"]
---

{{< figure src="./me.jpg" width=300px caption="That is me (and my kitten)" >}}

Hello! My name is Caio Volpato (caioau), from Brazil.

I have Bachelor's degree in Computational and Applied Mathematics (also did a lot of courses in statistics) at [Institute of Mathematics, Statistics and Scientific Computation](https://www.ime.unicamp.br/) (IMECC) of the [University of Campinas](https://www.unicamp.br/unicamp/english) (Unicamp), a post graduate course in Data Science and another one on software engineering.

I been working with Data Science and DevOps.

I'm passionate about free/libre software, cyber security, digital privacy and hacker culture.

Since 2019, I'm a core member of the Casa Hacker hackerspace ([casahacker.org](https://casahacker.org/)). We are a young hackerspace with the mission of empowering local communities through technology.

In Nov/2020 with some friends we created a science communication medium page, we write about computer science, maths and data science, in brazilian portuguese (pt-br). It's called [computing art](https://medium.com/computando-arte).

I'm also a volunteer at the [Tor Project](https://www.torproject.org/) operating some [Relays](/../tor-relays/), doing translations and other demands from global-south community.

As June of 2020, the best video that anyone can learn what this topics is all about is [Data](https://youtu.be/fCUTX1jurJ4) made by Abigail Thorn in her **Philosophy Tube** YouTube channel.

Another great video is [Internet Expert Debunks Cybersecurity Myths | WIRED](https://youtu.be/cQI0O7xdNOU) where Eva Galperin gives awesome advices on how to stay secure in the internet.


## Friends with webpages:

* [Gus](https://gus.computer/): [RSS feed](https://gus.computer//feeds/all.atom.xml)
* [Anna e só](https://anna.flourishing.stream/): [RSS feed](https://anna.flourishing.stream/index.xml)
* [Douglas Esteves](https://douglasesteves.eng.br/): [RSS feed](https://douglasesteves.eng.br/feed/)
* [Caroline Medeiros](http://www.craftermath.com.br/): [RSS feed](https://www.craftermath.com.br/feed.xml)
* [Vinícius Aires](https://v4ires.github.io/): [RSS feed](https://v4ires.github.io/feed.xml)
* [Gabriel Stefanini](https://g4brielvs.me/): [RSS feed](https://g4brielvs.me/feed.xml)
* [Luan Nico](https://luan.xyz/)

## Hobbies:

* Watching sci-fi movies and tv shows, I love gattaca and severance tv show
* Going to the gym

## Contact

You can reach me via:

### Email

My email address is:

`caio *dot* volpato *at* riseup *dot* net`

If you want to encrypt the message you can use my [PGP key](https://keybase.io/caioau/key.asc), here's the fingerprint:

`rsa4096/0xEFF5B2E180F294CE: 210B C5A4 14FD 9274 6B6A 250E EFF5 B2E1 80F2 94CE`

Tip: If you don't know how to encrypt a message using PGP, you can use [keybase-encrypt](https://keybase.io/encrypt#caioau): Put your message there and hit the `Encrypt` button and paste the encrypted message in the email.

### Signal

My [Signal](https://signal.org/) username is below:

`caioau.42`

**Don't forget to set the [disappearing messages](https://support.signal.org/hc/en-us/articles/360007320771-Set-and-manage-disappearing-messages) timer to 1 week.**


### Mastodon

[@caioau@mastodon.social](https://mastodon.social/@caioau)
