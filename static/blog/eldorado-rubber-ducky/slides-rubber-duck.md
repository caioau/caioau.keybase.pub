---
marp: true
theme: gaia
paginate: true
---

<!-- Center images -->
<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

# Ataques rubber ducky e mitigações

Chapter de computação visual

Caio Volpato

04 de janeiro de 2022

---

# Resumo

Hoje vamos falar dos ataques rubber duck e possíveis mitigações.

Em resumo: Esses ataques são dispositivos USB que se comportam como um teclado, que digitam rapidamente, para instalar algum programa malicioso.

---

## Agenda

1. Demo
2. Alguns devices de rubber ducky
3. Mitigações possíveis
4. Uma aplicação
5. Bonus: usando uma yubikey como rubber ducky.

---

## Demo

Nessa demo vamos ver como:

1. gerar um apk malicioso.
2. programar um arduino para atuar como teclado
3. plugar o arduino no celular e instalar o apk
4. ter acesso ao celular remotamente

Link da demo: [github.com/caioau/badUSB-Targeting-Android](https://github.com/caioau/badUSB-Targeting-Android)

---

## Quão grave é o ataque?

Com o apk instalado o atacante terá um shell que consegue:

* Acessar contatos, arquivos e fotos
* Acessar localização
* Abrir a câmera e microfone

---

## Quão grave é o ataque ? Limitações

Felizmente, tem com algumas limitações:

- A sessão não é persistente, depois do reboot o app deixa de rodar.
- O app por mais que não seja listado no launcher ele aparece na lista de apps
- Os atalhos são  muito específicos para cada versão de android e marca, dificultando um ataque genérico.
  - Exemplo: [(Jan/2017) Trump's Still Using His Old Android Phone. That's Very, Very Risky](https://www.wired.com/2017/01/trump-android-phone-security-threat/)

---

## Alguns devices: rubber Ducky original da hak5

![h:500px center](orig-rubber-duck.webp)

---

#### Alguns devices: flipper zero

![h:600px center](flipper_zero.png)

---

## alguns devices: omg cable ([o.mg.lol](https://o.mg.lol/))

![h:600px center](omg-cable-xray.jpg)

---

## Alguns devices: arduino ([franzininho.com.br](https://franzininho.com.br/))

![h:600px center](franzininho.png)

---

## Mitigações: usb data blocker

Desvantagem: Sem os pinos de dados, nao funciona o fast charging.

![h:600px center](usb_data_blocker.jpg)

---

## Mitigações: usbguard

Usbguard é um daemon para Linux que gerencia quais devices USB são autorizados, baseado em listas e regras.

![h:400px center](usbguard.png)

---

## Mitigações: QubesOS

QuebesOS é um sistema operacional baseado em Linux que compartimentaliza os usos do computador em diversas maquinas virtuais.

Website: [qubes-os.org](https://www.qubes-os.org/)

---

## Arquitetura do QubesOS

![h:500px center](qubes-trust-level-architecture.png)

---

## Screenshot qubesOS

![h:550px center](qubes-desktop.png)

---

### Aplicação: Uso como cheat em games

Alguns sistemas anti cheat para rodar em modo privilegiado, em ring 0, a nivel de kernel, o que fez alguns cheats irem para o hardware, para não serem detectados.

* macros do teclado
* compesador de recuo
* aimbot

[A closer look at Valorant's always-on anti-cheat system](https://www.engadget.com/valorant-vanguard-riot-games-security-interview-video-170025435.html)

[HARDWARE para TRAPACEAR EM JOGOS: Os cheats quase indetectáveis (Ft. Julio Della Flora)](https://www.youtube.com/watch?v=1zmLfUzu3S8)

---

![h:560px center](aimbot.png)

[Aimbot de Hardware para CSGO? Existe? (Julio Della Flora)](https://www.youtube.com/watch?v=GOdpUrDS7U0)


---

## Bonus: Usando a yubikey como rubber Ducky

O que é a yubikey?

Yubikey é a marca mais conhecida de chaves físicas de segurança, provendo um mecanismo adicional de segurança em 2 fatores.

![h:320px center](YubiKey-5-family-new-photos-web@2x-768x672.png)

---

## Bonus: Usando a yubikey como rubber Ducky

Dentre os protocolos suportados a yubikey tem o Yubico OTP, que são inputados via teclado.

A yubikey não é um device estranho a ser plugado em uma porta USB, levantando menos suspeitas.

O blogpost: [How to Weaponize the Yubikey](https://www.blackhillsinfosec.com/how-to-weaponize-the-yubikey/) descreve como configurar a yubikey para esse uso.
